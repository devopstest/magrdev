package com.browser;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AkveoTestChrome {
	private WebDriver driver;		
	@Test(priority = 0)		
	public void HomePageTitle() throws InterruptedException {	
		System.out.println("test started");
		driver.get("https://www.akveo.com/ngx-admin/");  
		Thread.sleep(20000);
		String title = driver.getTitle();
		driver.manage().window().maximize();
		//driver.findElement(By.className("menu-title")).click();
		Thread.sleep(10000);
		
	}	
	
	@Test(priority = 1)
	public void ElectricityConsumed() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/div/div/nb-layout-column/ngx-dashboard/div[2]/div[2]/ngx-electricity/nb-card/div[1]/nb-tabset/ul/li[2]/a")).click();
		Thread.sleep(20000);
		String month=driver.findElement(By.className("month")).getText();
		assertEquals("Jan", month);
		String Consumed=driver.findElement(By.className("results")).getText();
		System.out.println("Electricity Consumed in "+month+" "+Consumed);
	}
	
	@Test(priority = 2)
	public void UIFeature_Icons() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[3]/a/i[2]")).click();
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[3]/ul/li[4]/a/span")).click();
		Thread.sleep(20000);
		
		
	}
	
	@Test(priority = 3)
	public void UIFeature_TypoGrapy() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[3]/ul/li[5]/a/span")).click();
		Thread.sleep(20000);
	}
	
	@Test(priority = 4)
	public void UIFeature_Model() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[3]/ul/li[4]/a/span")).click();
		Thread.sleep(20000);
	}
	
	
	@Test(priority = 5)
	public void Form_Layout() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[4]/a/i[2]")).click();
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[4]/ul/li[2]/a/span")).click();
		Thread.sleep(20000);
	}
	
	@Test(priority = 6)
	public void Map_Google() throws InterruptedException {
	driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[6]/a/span")).click();
	driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[6]/ul/li[1]/a/span")).click();
	Thread.sleep(20000);
	}
	
	@Test(priority = 7)
	public void Map_Leaflet() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[6]/ul/li[2]/a/span")).click();
		Thread.sleep(20000);
	}
	
	@Test(priority = 8)
	public void Map_Bubble() throws InterruptedException {
		driver.findElement(By.xpath("html/body/ngx-app/ngx-pages/ngx-sample-layout/nb-layout/div/div/div/nb-sidebar[1]/div/div/nb-menu/ul/li[6]/ul/li[3]/a/span")).click();
		Thread.sleep(20000);
	}
	
	@BeforeTest
	public void beforeTest() {	
			System.setProperty("webdriver.chrome.driver","/usr/local/bin/chromedriver" );
			ChromeOptions chromeOptions = new ChromeOptions();
	      //chromeOptions.setBinary("/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary");
			//desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
			chromeOptions.addArguments("--headless");

			driver = new ChromeDriver(chromeOptions);
			
		
		//System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver" );
		/*driver = new InternetExplorerDriver();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);*/
		//driver = new ChromeDriver();
	   //driver = new FirefoxDriver(); 
		
		
	    
	}	
	
	@AfterTest
	public void afterTest() {
		driver.quit();			
	}	
}
