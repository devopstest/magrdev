package com.browser;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class Akveo {

	public static void main(String[] args) {
		String[] broswer=args;
		TestListenerAdapter tla = new TestListenerAdapter();
		 org.testng.TestNG testng = new TestNG();
		 if (broswer[0].equalsIgnoreCase("chrome")) {
			 testng.setTestClasses(new Class[] { AkveoTestChrome.class });
			 testng.addListener(tla);
			 testng.run();
		}
		else if (broswer[0].equalsIgnoreCase("IE")) {
			testng.setTestClasses(new Class[] { AkveoTestIE.class });
			testng.addListener(tla);
			testng.run();
		}
		else{
			testng.setTestClasses(new Class[] { AkveoTest.class });
			testng.addListener(tla);
			testng.run();
		}
	}

}
